const express = require('express')
const app = express()
const port = 3000

/**
 * This function adds two numbers together
 * @param {Number} a 
 * @param {Number} b 
 * @returns {Number}
 */
const add = (a,b) => {
    return a + b;
}
/** Path to add function */
app.get('/add/', (req, res) => {  
   const x = add(1,2)
   res.send(`sum:  ${x}`);
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})